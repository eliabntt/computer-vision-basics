#ifndef LAB4CLASS_H
#define LAB4CLASS_H
#include <exception>
#include <string>
#include <vector>
#include <opencv2/core/mat.hpp>

class lab4class : public std::exception
{
public:

	lab4class(const std::string& path) throw(std::exception);
	lab4class();
	void doCanny(const int& threshold1, const int& threshold2, const bool& createTrack);
	std::vector<cv::Vec2f> doHough(const int& theta, const int& threshold, const bool& createTrack);
	std::vector<cv::Vec3f> doCirc(const int& dp, const int& threshold, const int& param1, const int& maxRadius, const bool& createTrack);
	void doDrawCirc(const int& selectedDp, const int& selectedThreshold, const int& selectedParam, const int& selectedRadius);
	void doDrawHough(const int& selectedTheta, const int& selectedThreshold);
	void canny(int);
	void circ(int);
	void lines(int);
	cv::Mat getCannied();
	cv::Mat getFinalImage();
private:
	cv::Mat img, final_img, blurred, cannied;
	int dp{}, theta{}, maxRadius{}, threshold{}, threshold2{}, param1{};
};

#endif // LAB4CLASS_H
