#include "lab4class.h"
#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/shape/hist_cost.hpp>

using namespace std;
using namespace cv;

/// <summary>
/// Static methods to create and manage trackbars
/// </summary>
static void s_circle(int, void * ptr) {
	lab4class *obj = (lab4class*)(ptr);
	obj->circ(0);
};
static void s_canny(int, void * ptr) {
	lab4class *obj = (lab4class*)(ptr);
	obj->canny(0);
};
static void s_lines(int, void * ptr) {
	lab4class *obj = (lab4class*)(ptr);
	obj->lines(0);
};

/// <summary>
/// Constructor of the class
/// </summary>
/// <param name="path">The path of the image</param>
lab4class::lab4class(const string& path) throw(exception)
{
	img = imread(path);
	if (img.empty())
		throw invalid_argument("Image not found exception");
	img.copyTo(final_img);
	cvtColor(img, cannied, CV_BGR2GRAY);
}

/// <summary>
/// Default constructor
/// </summary>
lab4class::lab4class()
{
	img = Mat(320, 240, CV_8UC3, Scalar(0, 0, 0));
	img.copyTo(final_img);
	cvtColor(img, cannied, CV_BGR2GRAY);
	cout << "nothing yet created" << endl;
}

/// <summary>
/// Return the cannied image
/// </summary>
/// <returns>Mat of the cannied image</returns>
Mat lab4class::getCannied() {
	return cannied;
}

/// <summary>
/// Do the canny to the image
/// </summary>
/// <param name="threshold1">The first threshold [1,800]</param>
/// <param name="threshold2">The second threshold [1,800]</param>
/// <param name="createTrack">True if you need the trackbar to tune the parameters</param>
void lab4class::doCanny(const int& threshold1, const int& threshold2, const bool& createTrack) {
	
	//If do not need to create the trackbar do directly the Canny
	if (!createTrack)
		Canny(img, cannied, threshold1, threshold2);
	
	else
	{
		namedWindow("Canny", WINDOW_AUTOSIZE);
		this->threshold = threshold1;
		this->threshold2 = threshold2;
		int max_thre = 800;
		createTrackbar("Threshold1", "Canny", &(this->threshold), max_thre, s_canny, this);
		createTrackbar("Threshold2", "Canny", &(this->threshold2), max_thre, s_canny, this);
		s_canny(0, this);
		waitKey(0);
		destroyAllWindows();
	}

}

/// <summary>
/// Manage the trackbars movements results
/// </summary>
/// <param name="value"></param>
void lab4class::canny(int)
{
	Mat result;
	
	try
	{
		Canny(img, result, threshold, threshold2, 3);
	}
	catch (const std::exception&)
	{
		img.copyTo(result);
		putText(result, "WRONG PARAMETERS", Point(0, result.size().height), FONT_HERSHEY_DUPLEX, 10, Scalar(0, 0, 255), 4);
	}
	imshow("Canny", result);
}

/// <summary>
/// Apply the hough method
/// </summary>
/// <param name="theta">Selected theta</param>
/// <param name="threshold">Selected threshold</param>
/// <param name="createTrack">True if you need to tune the parameters</param>
/// <returns>The vector with the informations of the lines</returns>
vector<Vec2f> lab4class::doHough(const int& theta, const int& threshold, const bool& createTrack) {
	
	vector<Vec2f> lines;
	if (!createTrack)
	{
		HoughLines(cannied, lines, 1, CV_PI / 180 * theta * 2 / 100, threshold);
	}
	else
	{
		namedWindow("Hough Lines", WINDOW_AUTOSIZE);
		this->theta = theta;
		this->threshold = threshold;
		int max_thre = 300;
		int max_theta = 800;
		createTrackbar("Theta", "Hough Lines", &this->theta, max_theta, s_lines, this);
		createTrackbar("Threshold", "Hough Lines", &this->threshold, max_thre, s_lines, this);
		s_lines(0, this);
		waitKey(0);
		destroyAllWindows();
	}

	return lines;
}

/// <summary>
/// Draw the area under the lines given from the hough method
/// </summary>
/// <param name="selectedTheta"></param>
/// <param name="selectedThreshold"></param>
void lab4class::doDrawHough(const int& selectedTheta, const int& selectedThreshold) {

	vector<Vec2f> lines = doHough(selectedTheta, selectedThreshold, false);

	double r[2] = { 0 }, a[2] = { 0 }, b[2] = { 0 };

	for (size_t i = 0; i < lines.size(); i++)
	{
		float rho = lines[i][0];
		float thetas = lines[i][1];
		a[i] = cos(thetas);
		b[i] = sin(thetas);
		r[i] = rho;
	}
	//todo check
	double x0 = cvRound(a[0] * r[0] + 1000 * (-b[0]));
	double x1 = cvRound(a[1] * r[1] + 1000 * (-b[1]));
	double x2 = cvRound(a[0] * r[0] - 1000 * (-b[0]));
	double x3 = cvRound(a[1] * r[1] - 1000 * (-b[1]));

	for (int z = max(0, (int)min(x0, x1)); z < min(cannied.size().width, (int)max(x2, x3)); z++)
	{
		double y1 = (-a[0] / b[0])*z + r[0] / b[0];
		double y2 = (-a[1] / b[1])*z + r[1] / b[1];
		//if inside the image at least one of the lines
		//TODO maybe improve
		if (((0 <= y1) &(y1 <= cannied.size().height)) | ((0 <= y2)& (y2 <= cannied.size().height)))
			line(final_img, Point(z, cannied.size().height), Point(z, (int)max(y1, y2)), Scalar(0, 0, 255), 3, 8);
	}

	imshow("Hough lines step", final_img);
	waitKey(0);
	destroyAllWindows();
}

/// <summary>
/// Manage the results of the trackbars for the Hough
/// </summary>
/// <param name="value"></param>
void lab4class::lines(int)
{
	vector<Vec2f> lines;
	Mat temp;
	cannied.copyTo(temp);
	try
	{
		HoughLines(cannied, lines, 1, CV_PI / 180 * theta * 2 / 100, threshold);
		for (size_t i = 0; i < lines.size(); i++)
		{
			float rho = lines[i][0];
			float thetas = lines[i][1];
			double a = cos(thetas), b = sin(thetas);
			double x0 = a*rho, y0 = b*rho;
			Point pt1(cvRound(x0 + 1000 * (-b)),
				cvRound(y0 + 1000 * (a)));
			Point pt2(cvRound(x0 - 1000 * (-b)),
				cvRound(y0 - 1000 * (a)));
			line(temp, pt1, pt2, Scalar(255, 255, 255), 3, 8);
		}
	}
	catch (const std::exception&)
	{
		putText(temp, "WRONG PARAMETERS", Point(0, temp.size().height), FONT_HERSHEY_DUPLEX, 1, Scalar(255, 255, 255), 4);
	}
	imshow("Hough Lines", temp);
}

/// <summary>
/// Apply the circ method
/// </summary>
/// <param name="dp"></param>
/// <param name="threshold"></param>
/// <param name="param1"></param>
/// <param name="maxRadius"></param>
/// <param name="createTrack">True if you need to tune the parameters</param>
/// <returns>The vector of selected circles</returns>
vector<Vec3f> lab4class::doCirc(const int& dp, const int& threshold, const int& param1, const int& maxRadius, const bool& createTrack) {
	vector<Vec3f> circ;
	int max_theta = 200;
	int max_thre = 200;
	int max_radius = 100;
	if (!createTrack)
	{
		GaussianBlur(cannied, blurred, Size(9, 9), 2, 2);
		HoughCircles(blurred, circ, HOUGH_GRADIENT, dp + 1, threshold, param1, 100, 0, maxRadius);
		return circ;
	}

	namedWindow("Hough Circ", WINDOW_AUTOSIZE);
	this->dp = dp;
	this->threshold = threshold;
	this->param1 = param1;
	this->maxRadius = maxRadius;
	createTrackbar("dp", "Hough Circ", &this->dp, max_theta, s_circle, this);
	createTrackbar("Min dist", "Hough Circ", &this->threshold, max_thre, s_circle, this);
	createTrackbar("Param 1", "Hough Circ", &this->param1, max_thre, s_circle, this);
	createTrackbar("Max radius", "Hough Circ", &this->maxRadius, max_radius, s_circle, this);
	s_circle(0, this);
	waitKey(0);
	destroyAllWindows();
	return circ;
}

/// <summary>
/// Draw the circle in the image
/// </summary>
/// <param name="selectedDp"></param>
/// <param name="selectedMinDist"></param>
/// <param name="selectedParam"></param>
/// <param name="selectedRadius"></param>
void lab4class::doDrawCirc(const int& selectedDp, const int& selectedMinDist, const int& selectedParam, const int& selectedRadius) {

	vector<Vec3f> circ = doCirc(selectedDp, selectedMinDist, selectedParam, selectedRadius, false);

	for (size_t i = 0; i < circ.size(); i++)
	{
		Point center(cvRound(circ[i][0]), cvRound(circ[i][1]));
		int radius = cvRound(circ[i][2]);
		// draw the circle outline
		circle(final_img, center, radius, Scalar(0, 255, 0), CV_FILLED, 8, 0);
	}

	imshow("Final image", final_img);
	waitKey(0);
	destroyAllWindows();
}

/// <summary>
/// Manage the trackbar results
/// </summary>
/// <param name="value"></param>
void lab4class::circ(int)
{
	Mat result;
	vector<Vec3f> lines;
	Mat temp;
	cannied.copyTo(temp);
	GaussianBlur(temp, temp, Size(9, 9), 2, 2);
	try
	{
		HoughCircles(temp, lines, HOUGH_GRADIENT, dp + 1, threshold, param1, 100, 0, maxRadius);
		for (size_t i = 0; i < lines.size(); i++)
		{
			Point center(cvRound(lines[i][0]), cvRound(lines[i][1]));
			int radius = cvRound(lines[i][2]);
			// draw the circle center
			circle(temp, center, 2, Scalar(255, 255, 255), -1, 8, 0);
			// draw the circle outline
			circle(temp, center, radius, Scalar(255, 255, 255), 3, 8, 0);
		}
	}
	
	catch (const std::exception&)
	{
		putText(temp, "WRONG PARAMETERS", Point(0, temp.size().height), FONT_HERSHEY_DUPLEX, 1, Scalar(255, 255, 255), 4);
	}
	
	imshow("Hough Circ", temp);
}

/// <summary>
/// Returns the image elaborated
/// </summary>
/// <returns>The image as Mat</returns>
Mat lab4class::getFinalImage()
{
	return final_img;
}