#include "lab4class.h"
#include <iostream>
#include <opencv2/highgui.hpp>


using namespace std;
using namespace cv;

/// <summary>
/// Check the validity of the input
/// </summary>
/// <param name="quest">The fixed string to be asked</param>
/// <param name="lower">the lower-1 admitted value</param>
/// <param name="upper">the upper+1 admitted value</param>
/// <returns></returns>
template <typename T>
static T check(string quest, T lower, T upper)
{
	bool finish = false;
	T value;
	while (!finish)
	{
		cout << quest << endl;
		cin >> value;
		if ((lower < value) & (value < upper))
			finish = true;
		else
		{
			cin.clear();
			cin.ignore();
		}
	}
	return value;
}

int main(int argc, char** argv)
{
	if (argc < 2 | argc > 2)
	{
		cout << "Not right # of arguments\n";
		cout << "Need the path of the picture(including name and extension)\n";
		cout << "press enter to finish.." << endl;
		cin.ignore();
		return -1;
	}
	if (argc == 2)
			cout << "# of arguments ok" << endl;
	
	lab4class obj;
	try
	{
		obj = lab4class(argv[1]);
	}
	catch (const std::exception& e)
	{
		cout << e.what() << endl;
		cin.ignore();
		return -1;
	}
	cout << "The suggested values works only if from the most reced canny operation all suggested values have been followed.\n";
	cout << "Feel free to experiment with the values. The images that will come up with the trackbar already have the \"right\" values pre-setted.\n";
	cout <<"\n" << "The first step is to do a Canny on the source image, set the thresholds and search the lines with the hough transform\n";
	cout << "You can always directly close the image and go for the suggested values\n";
	cout <<"\n" << "The line detected must be 2 incident lines otherwise bad things will probably happen" << endl;
	cout << "Press [Return] to go on";
	cin.ignore();
	string quest;
	bool end = false;

	int max_thre = 800;
	int max_theta = 800;
	int max_radius = 200;

	while (!end)
	{
		int threshold1 = 800;
		int threshold2 = 611;

		obj.doCanny(threshold1, threshold2, true);
		quest = "Which threshold1 do you want to use?[suggested 800]";
		threshold1 = check<int>(quest, 100, max_thre + 1);
		quest = "Which threshold2 do you want to use?[suggested 611]";
		threshold2 = check<int>(quest, 100, max_thre + 1);

		obj.doCanny(threshold1, threshold2, false);

		obj.doHough(557, 100, true);
		string sTest = "Y";
		cout << "Do you want to retry from canny? Y/n" << endl;
		cin >> sTest;
		end = ((sTest == "n") | (sTest == "N")) ? true : false;
	}
	cout <<"\n";

	quest = "Which theta do you want to use?[suggested 557]";
	int selected_theta = check<int>(quest, 1, max_theta + 1);

	quest = "Which threshold do you want to use?[suggested 100]";
	int selected_thre = check<int>(quest, 1, max_thre + 1);

	obj.doDrawHough(selected_theta, selected_thre);

	cout <<"\n \n \n";
	cout << "Now again we redo the Canny since we have to search for circles and not for lines. You may be want to modify the previous thresholds" << endl;
	cin.ignore();
	end = false;
	while (!end)
	{

		int threshold1 = 800;
		int threshold2 = 800;

		obj.doCanny(threshold1, threshold2, true);
		quest = "Which threshold1 do you want to use?[suggested 800]";
		threshold1 = check<int>(quest, 100, max_thre + 1);
		quest = "Which threshold2 do you want to use?[suggested 800]";
		threshold2 = check<int>(quest, 100, max_thre + 1);

		obj.doCanny(threshold1, threshold2, false);
		obj.doCirc(106, 100, 130, 38, true);

		string sTest = "Y";
		cout << "Do you want to retry from canny? Y/n" << endl;
		cin >> sTest;
		end = ((sTest == "n") | (sTest == "N")) ? true : false;
	}

	quest = "Which dp do you want to use?[suggested 106]";
	int selected_dp = check<int>(quest, 1, max_theta + 1);

	quest = "Which minDist do you want to use?[suggested 100]";
	int selected_minDist = check<int>(quest, 1, max_thre + 1);

	quest = "Which param1 do you want to use?[suggested 130]";
	int selected_param1 = check<int>(quest, 1, max_thre + 1);

	quest = "Which maxRadius do you want to use?[suggested 38]";
	int selected_maxRadius = check<int>(quest, 1, max_radius + 1);

	obj.doDrawCirc(selected_dp, selected_minDist, selected_param1, selected_maxRadius);
	imwrite("Final_result.jpg", obj.getFinalImage());
	return 0;
}