cmake_minimum_required(VERSION 2.8)
project(CameraCalib)

find_package(OpenCV REQUIRED)

include_directories(${OpenCV_INCLUDE_DIRS})

add_executable(${PROJECT_NAME} src/CameraCalib.cpp)
target_link_libraries(${PROJECT_NAME} ${OpenCV_LIBS})
