#include "stdafx.h"

#include <iostream>
#include <opencv2/opencv.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui.hpp"

using namespace std;
using namespace cv;


/// <summary>
/// Import images from a given path and extension.
/// </summary>
/// <param name="path">The path were to search images</param>
/// <param name="extension">The exetension of the images</param>
/// <param name="photos">The vector where to store the name of the images</param>
/// <returns>Mat vector to be used with opeenCV</returns>
inline vector<Mat> getImg(string path, string extension, vector<string>* photos, vector<double>* variance)
{
	vector<Mat> data;
	vector<String> res;
	String loc = path + "/*" + extension;
	//with glob one can find the files within the location(note the wildcard *)
	//and put the result on res. false/true is for recursive
	glob(loc, res, false);
	for (size_t k = 0; k < res.size(); ++k)
	{
		Mat im = imread(res[k]), lapl;
		Scalar mean, stddev;
		if (im.empty()) continue;
		cvtColor(im, im, CV_BGR2GRAY);
		data.push_back(im);
		//save only the picture name with extension
		photos->push_back(res[k].substr(path.length() + 1));

		Laplacian(im, lapl, CV_64F);
		meanStdDev(lapl, mean, stddev, Mat());
		variance->push_back(stddev.val[0] * stddev.val[0]);
	}
	return data;
}


template <typename T>
/// <summary>
/// Check the validity of the input
/// </summary>
/// <param name="quest">The fixed string to be asked</param>
/// <param name="lower">the lower-1 admitted value</param>
/// <param name="upper">the upper+1 admitted value</param>
/// <returns></returns>
static T check(string quest, T lower, T upper)
{
	bool finish = false;
	T value;
	while (!finish)
	{
		cout << quest << endl;
		cin >> value;
		if ((lower < value) & (value < upper))
			finish = true;
		else
		{
			cin.clear();
			cin.ignore();
		}
	}
	return value;
}

//how much to wait when showing an images
const int waitTime = 0;
/// /// <summary>
/// Calibrating a camera given a set of pictures. Test the calibration with a test image.
/// Need 3 parameters, the path of the images(without name, only the folder) to use for the calibration, their extension
/// and the picture(comprehensive if needed of the path) to test the result. If you don't have that provide a fake param,
/// the worst picture will be used instead.
/// </summary>


int main(int argc, char** argv)
{
	//check the inputs
	if (argc < 4)
	{
		cout << "Not enough argument" << endl;
		cout << "Need the path with the checkboards, the extension(jpeg != jpg), and the complete path of the test picture(including name and extension)" << endl;
		cout << "press any key to finish.." << endl;
		cin.ignore();
		return -1;
	}
	else
		if (argc == 4)
			cout << "# of arguments ok" << endl;
		else
		{
			cout << "too much arguments" << endl;
			cout << "Need the path with the checkboards, the extension, and the complete path of the test picture(including name and extension)" << endl;
			cout << "press any key to finish.." << endl;
			cin.ignore();
			return -1;
		}

	//load the images and check if something is really obtained.
	vector<string> names;
	vector<double> variance;
	vector<Mat> data;
	data = getImg(argv[1], argv[2], &names, &variance);
	if (data.size() == 0)
	{
		cout << "Not picture detected" << endl;
		cout << "Need the path with the checkboards and the right extension, I cannot tell what is the problem" << endl;
		cout << "press any key to finish.." << endl;
		cin.ignore();
		return -1;
	}
	else
	{
		cout << "Found " << data.size() << " pictures" << endl;
	}
	const int size = data.size();

	int  width, height, winSize;
	float squareSize;
	//get the parameters from the user
	string quest = "insert # of crossing in width direction";
	width = check<int>(quest, 0, numeric_limits<int>::max());
	quest = "in height";
	height = check<int>(quest, 0, numeric_limits<int>::max());
	quest = "insert size in m of the edge of the square";
	squareSize = check<double>(quest, 0, numeric_limits<double>::max());

	quest = "insert winsize(suggestion: stay low, a number between 2 and 6 usually is good)";
	winSize = check<int>(quest, 0, numeric_limits<int>::max());

	string sTest = "Y";
	cout << "Do you want to test the findChessboard param? Y/n" << endl;
	cin >> sTest;
	bool test = (sTest == "n") | (sTest == "N") ? false : true;

	int maxrun = 16;
	int minrun = -1;
	while (!(maxrun <= 15 & maxrun >= 0))
		if (!test)
		{
			quest = "Which flag for findChessboard? [<=15]";
			maxrun = check<int>(quest, -1, 16);
			minrun = maxrun;
		}
		else
		{
			maxrun = 15;
			minrun = 0;
			cout << "The threshold for automatic detection of blurred image is set at 35 as result of a test on various value done in a previous computation" << endl;
		}

	vector<vector<double>> errors(maxrun + 1);
	vector<Mat> cameraMatrix(maxrun + 1), distortionCoefficients(maxrun + 1);
	Mat split_1, split_2;
	vector<double> rmss;
	for (int chessparam = minrun; chessparam <= maxrun; chessparam++)
	{
		//create (most of) variables needed
		vector<Mat> rotationVectors, translationVectors;
		vector<double> stdIntr, stdExtr;
		vector<vector<Point2f>> corners;
		vector<Point2f> temp;
		vector<vector<Point3f> > objectPoints;
		vector<Point3f> points;



		Size patternsize(width, height);
		//generate the vector of points once and for all
		for (int i = 0; i < patternsize.height; i++)
		{
			for (int j = 0; j < patternsize.width; j++)
			{
				points.push_back(Point3f(float(j*squareSize),
					float(i*squareSize), 0));
			}
		}

		//find chessboardcorners, draw the corners and use subpix
		int count = 0;
		bool patternfound = false;

		double threshold;
		bool finish = false;
		double max_variance = max_element(variance.begin(), variance.end()) - variance.begin(), min_variance = min_element(variance.begin(), variance.end()) - variance.begin();
		if (!test)
		{
			quest = "What is the threshold for blurred images?\n Note that the max variance is " + to_string((int)floor(max_variance)) + " and the min is " + to_string((int)ceil(min_variance)) + " [suggested 35 or 44(maybe too high)]\n";
			threshold = check<double>(quest, min_variance, max_variance + 1);
		}
		else
		{
			threshold = 35;
		}


		for (int i = 0; i < size; i++)
		{
			//todo it's interstengin to play with kernel size and threshold, since it vary dataset to dataset
			if (variance.at(i) >= threshold)
				patternfound = findChessboardCorners(data[i], patternsize, temp, chessparam);
			else
				patternfound = false;
			if (patternfound)
			{
				//explicitly we can have a maximum number of iterations or a given accuracy to be reached
				cornerSubPix(data[i], temp, Size(winSize, winSize), Size(-1, -1), TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1));
				if (!test) drawChessboardCorners(data[i], patternsize, Mat(temp), patternfound); //draw only if has to be used only one time
				corners.push_back(temp);
				objectPoints.push_back(points);
				count += 1;
			}
			else
			{
				if (!test) cout << "for picture #" << names.at(i) << " not found a chessboard" << endl;
			}
		}

		cout << "found " << count << " chessboards in total" << endl;
		if (count == 0)
		{
			cout << "No chessboard found, terminating. Check the arguments given as input." << endl;
			cout << "Press a key to exit..." << endl;
			cin.ignore();
			return -1;
		}
		cameraMatrix.at(chessparam) = Mat::eye(3, 3, CV_64F);
		distortionCoefficients.at(chessparam) = Mat::zeros(8, 1, CV_64F);

		double rms = calibrateCamera(objectPoints, corners, data[0].size(), cameraMatrix.at(chessparam), distortionCoefficients.at(chessparam), rotationVectors, translationVectors, stdIntr, stdExtr, errors.at(chessparam), CV_CALIB_RATIONAL_MODEL);
		rmss.push_back(rms);
		cout << "The overall mean RMS is: " << rms << " for run " << chessparam << endl;
	}


	int best_coeff = test ? min_element(rmss.begin(), rmss.end()) - rmss.begin() : maxrun;

	double tot = 0;
	for (int i = 0; i < errors.at(best_coeff).size(); i++)
	{
		tot = tot + errors.at(best_coeff).at(i);
	}
	if (test) cout << "The overall mean RMS is: " << rmss.at(best_coeff) << "for run " << best_coeff << endl;

	cout << "The mean of the RMS error for each pattern view is " << tot / errors.at(best_coeff).size() << errors.size() << endl;
	cout << "the picture that performs better is the " << min_element(errors.at(best_coeff).begin(), errors.at(best_coeff).end()) - errors.at(best_coeff).begin() << " so " << names.at(min_element(errors.at(best_coeff).begin(), errors.at(best_coeff).end()) - errors.at(best_coeff).begin()) << endl;
	cout << "the picture that performs worst is the " << max_element(errors.at(best_coeff).begin(), errors.at(best_coeff).end()) - errors.at(best_coeff).begin() << " so " << names.at(max_element(errors.at(best_coeff).begin(), errors.at(best_coeff).end()) - errors.at(best_coeff).begin()) << endl;
	imshow("Best", data[min_element(errors.at(best_coeff).begin(), errors.at(best_coeff).end()) - errors.at(best_coeff).begin()]);
	imshow("Worst", data[max_element(errors.at(best_coeff).begin(), errors.at(best_coeff).end()) - errors.at(best_coeff).begin()]);
	waitKey(waitTime);
	destroyWindow("Best");
	destroyWindow("Worst");
	system("pause");

	Mat testImg, undistImg, map1, map2;
	testImg = imread(argv[3]);
	if (testImg.empty())
	{
		cout << "test img not found, go directly with worst img" << endl;
	}
	else
	{	//undistort the test image
		initUndistortRectifyMap(cameraMatrix.at(best_coeff), distortionCoefficients.at(best_coeff), Mat(), cameraMatrix.at(best_coeff), testImg.size(), CV_32FC1, map1, map2);
		remap(testImg, undistImg, map1, map2, INTER_CUBIC);

		//imshow("Distorted image", testImg);
		//imshow("Corrected image", undistImg);

		//resize(to half) and split in the same view
		resize(testImg, split_1, Size(), 0.5, 0.5);
		resize(undistImg, split_2, Size(), 0.5, 0.5);
		Mat final_mat(split_1.size().height, split_1.size().width * 2, CV_8UC3);
		split_1.copyTo(final_mat(Rect(0, 0, split_1.size().width, split_1.size().height)));
		split_2.copyTo(final_mat(Rect(split_1.size().width, 0, split_1.size().width, split_1.size().height)));
		imshow("SplittedView", final_mat);
		imwrite("SplittedTest.jpg", final_mat);
		waitKey(waitTime);
		destroyAllWindows();
	}

	//unistort the worst image
	string worst = argv[1];
	worst += '\\';
	worst += names.at(max_element(errors.at(best_coeff).begin(), errors.at(best_coeff).end()) - errors.at(best_coeff).begin());
	Mat worst_img = imread(worst);

	initUndistortRectifyMap(cameraMatrix.at(best_coeff), distortionCoefficients.at(best_coeff), Mat(), cameraMatrix.at(best_coeff), worst_img.size(), CV_32FC1, map1, map2);
	remap(worst_img, undistImg, map1, map2, INTER_CUBIC);


	resize(worst_img, split_1, Size(), 0.5, 0.5);
	resize(undistImg, split_2, Size(), 0.5, 0.5);
	Mat final_mat(split_1.size().height, split_1.size().width * 2, CV_8UC3);
	split_1.copyTo(final_mat(Rect(0, 0, split_1.size().width, split_1.size().height)));
	split_2.copyTo(final_mat(Rect(split_1.size().width, 0, split_1.size().width, split_1.size().height)));
	imshow("SplittedView", final_mat);
	imwrite("SplittedWorst" + names.at(max_element(errors.at(best_coeff).begin(), errors.at(best_coeff).end()) - errors.at(best_coeff).begin()).substr(2) + ".jpg", final_mat);
	cout << names.at(max_element(errors.at(best_coeff).begin(), errors.at(best_coeff).end()) - errors.at(best_coeff).begin()).substr(2) << endl;
	waitKey(waitTime);
	destroyAllWindows();

	cout << "press any key to finish.." << endl;
	cin.ignore();
	return 0;
}