Mini-projects for the course on computer vision based on `OpenCV` and `C++`.

Every folder contains the assignment, the code and a short report mainly on the results.

The topics covered are:

* Calibration
* Car Detection
* Histograms normalization and visualizationù
* Filters and morpholigal operations
* Object recognition
* Panorama reconstruction
* Hough transform and edge detection

Most of them are at a very basic level since they are just insight on the subject and on `OpenCV`.


##Usage
You can easily use `cmake` to build the code.
The images can be found inside the zip files that you can found in each folder.

##Dependencies
To get `OpenCV` maybe with `CUDA` you can follow: .....


(C) 2018 Elia Bonetto