#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include "ObjectDetection.h"

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{

	if (argc < 3 | argc > 3)
	{
		cout << "Not right # of arguments\n";
		cout << "Need the path with the images, the extension(jpeg != jpg)\n";
		cout << "press any key to finish.." << endl;
		cin.ignore();
		return -1;
	}
	if (argc == 3)
		cout << "# of arguments ok" << endl;

	vector<String> input, comparison;
	string path = argv[1];
	string extension = argv[2];
	String obj = path + "/obj*" + extension;
	String scene = path + "/scene*" + extension;
	
	//with glob one can find the files within the location(note the wildcard *)
	//and put the result on res. false/true is for recursive
	glob(obj, input, false);
	glob(scene, comparison, false);

	//invert the two cycle
	for (size_t j = 0; j < comparison.size(); j++)
	{
		ObjectDetection scene_img(imread(comparison[j], IMREAD_GRAYSCALE));
		scene_img.sifting();
		try
		{
			
			for (size_t k = 0; k < input.size(); ++k)
			{
				ObjectDetection object_img(imread(input[k], IMREAD_GRAYSCALE));
				object_img.sifting();
				object_img.matching(scene_img.get_key_points(), scene_img.get_descriptors(), scene_img.getImg());
				object_img.findHomo(scene_img.get_key_points());
				object_img.drawBoxing();
			}
		}
		catch (...)
		{
		}
	}
    return 0;
}

