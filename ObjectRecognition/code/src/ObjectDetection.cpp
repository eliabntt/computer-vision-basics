#include "ObjectDetection.h"
 #include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/videostab/wobble_suppression.hpp>

using namespace cv;
using namespace std;

/// <summary>
/// Check the validity of the input
/// </summary>
/// <param name="quest">The fixed string to be asked</param>
/// <param name="lower">the lower-1 admitted value</param>
/// <param name="upper">the upper+1 admitted value</param>
/// <returns></returns>
template <typename T>
static T check(string quest, T lower, T upper)
{
	bool finish = false;
	T value;
	while (!finish)
	{
		cout << quest << endl;
		cin >> value;
		if ((lower < value) & (value < upper))
			finish = true;
		else
		{
			cin.clear();
			cin.ignore();
		}
	}
	return value;
}

ObjectDetection::ObjectDetection()
= default;


ObjectDetection::~ObjectDetection()
= default;

/// <summary>
/// Constructor to be used
/// </summary>
/// <param name="input_image">The input image to be used</param>
ObjectDetection::ObjectDetection(cv::Mat input_image)
{
	img = input_image;
}

/// <summary>
/// Get image given the index. Throw IndexOutOfRangeException.
/// </summary>
/// <param name="index">the index of the image</param>
/// <returns>The image Mat</returns>
Mat ObjectDetection::getImg() throw (exception)
{
	return img;
}


/// <summary>
/// Do the sifting procedure on the object image
/// </summary>
/// <returns></returns>
void ObjectDetection::sifting() throw (exception)
{
	//do the sift procedure
	Ptr<xfeatures2d::SIFT> sift_1 = xfeatures2d::SIFT::create();
	sift_1->detectAndCompute(img, noArray(), key_points, descriptors);

	Mat feature;
	drawKeypoints(img, key_points, feature);
	imshow("Img keypoints", feature);
	waitKey(0);
	destroyAllWindows();

	//release the Ptr
	sift_1.release();
}

/// <summary>
/// Do the match between the scene image information and the object image(the object's image)
/// </summary>
/// <param name="scene_img_key_points">The keypoints of the scene img.</param>
/// <param name="scene_img_descriptors">The descriptors of the scene img.</param>
/// <param name="scene_img">The scene img.</param>
/// <returns></returns>
void ObjectDetection::matching(vector<KeyPoint> scene_img_key_points, Mat scene_img_descriptors, Mat scene_img) throw(exception)
{


	Ptr<BFMatcher> matcher = BFMatcher::create(NORM_L2, false);
	vector<DMatch> matches;
	matcher->match(descriptors, scene_img_descriptors, matches, noArray());

	//double min_thre(1), max_thre(100);
	//string quest = "Which is the weight to apply to the min distance?[>" + to_string(min_thre) + " and <" + to_string(max_thre) + "]";
	//double thre = check<double>(quest, min_thre, max_thre);
	//refined = refine(matches, thre);

	//with threshold 16 for dataset2 ?4? errors over 16 imgs.(some we have a detection but not correct bounds), 7th maybe lower ,9 11 14 15 16 -> with 99
	//with 5 100% in dataset3

	refined = matches;

	drawMatches(img, key_points, scene_img, scene_img_key_points, refined, img_match, DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

	matcher.release();
}


/// <summary>
/// Do the refinment. Compute the min distance then compare to a multiplicative factor.
/// </summary>
/// <param name="input_DMatch">The vector of matches</param>
/// <param name="ratio">The ratio to be keeped</param>
/// <returns>The new vector</returns>
vector<DMatch> ObjectDetection::refine(vector<DMatch> input_DMatch, double ratio)
{
	double min_distance = 100;
	for (auto& i : input_DMatch)
	{
		if (i.distance < min_distance)
			min_distance = i.distance;
	}

	vector<DMatch> output;
	for (auto& i : input_DMatch)
	{
		if (i.distance < min_distance*ratio)
			output.push_back(i);
	}
	return output;
}

/// <summary>
/// Returns the key points of the image
/// </summary>
/// <returns></returns>
std::vector<cv::KeyPoint> ObjectDetection::get_key_points()
{
	return key_points;
}

/// <summary>
/// Return the descriptors of the image
/// </summary>
/// <returns></returns>
cv::Mat ObjectDetection::get_descriptors()
{
	return descriptors;
}

/// <summary>
/// compute the homography between the object and the scene keypoints
/// </summary>
/// <param name="scene_keypoints"></param>
void ObjectDetection::findHomo(vector<KeyPoint> scene_keypoints)
{
	std::vector<Point2f> img_1;
	std::vector<Point2f> img_2;
	for (auto& i : refined)
	{
		img_1.push_back(key_points[i.queryIdx].pt);
		img_2.push_back(scene_keypoints[i.trainIdx].pt);
	}
	homo_result = findHomography(img_1, img_2, RANSAC);
}

/// <summary>
/// Draw the boxing and show the resulting image.
/// </summary>
void ObjectDetection::drawBoxing()
{
	vector<Point2f> obj_corners = {
						Point2f(0, 0),
						Point2f(img.cols, 0),
						Point2f(img.cols, img.rows),
						Point2f(0, img.rows) };

	std::vector<Point2f> scene_corners(4);

	perspectiveTransform(obj_corners, scene_corners, homo_result);
	for (int i = 0; i < 4; i++)
		line(img_match, scene_corners[i] + Point2f(img.cols, 0), scene_corners[(i + 1) % 4] + Point2f(img.cols, 0), Scalar(0, 255, 0), 4);

	imshow("Result of the couple", img_match);
	waitKey(0);
	destroyAllWindows();
}
