#ifndef OBJECTDETECTION_H
#define OBJECTDETECTION_H
#include <opencv2/core/mat.hpp>

class ObjectDetection
{
public:
	//constructors
	ObjectDetection(cv::Mat input_image);
	ObjectDetection();
	~ObjectDetection();

	//main methods
	void sifting() throw (std::exception);
	
	void matching(std::vector<cv::KeyPoint> key_points_2, cv::Mat descriptors_2, cv::Mat scene_img) throw(std::exception);
	
	void findHomo(std::vector<cv::KeyPoint> scene_keypoints);
	
	void drawBoxing();

	//getter
	std::vector<cv::KeyPoint> get_key_points();
	cv::Mat get_descriptors();
	cv::Mat getImg() throw (std::exception);

private:

	//internal method
	std::vector<cv::DMatch> refine(std::vector<cv::DMatch> input_DMatch, double ratio);
	
	//members
	cv::Mat img, img_match, descriptors, homo_result;
	std::vector<cv::KeyPoint> key_points;
	std::vector<cv::DMatch> refined;
};

#endif // OBJECTDETECTION_H
