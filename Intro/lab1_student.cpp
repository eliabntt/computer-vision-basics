// include openCV and standard headers
#include <iostream>
#include <opencv2/opencv.hpp>

// window size
#define NEIGHBORHOOD_SIZE 9

// Thresholds on color differences
#define THRESH_R 50
#define THRESH_G 50
#define THRESH_B 50
#define THRESH_H 10

using namespace std;
using namespace cv;

// called when mouse is clicked
void onMouse( int event, int x, int y, int f, void* userdata){

  // If the left button is pressed
  if (event == cv::EVENT_LBUTTONDOWN)
  {
    
    // Retrieving the image from the main 
    // How to get the data from void* userdata ? 
	// 1) Need a cast from void* back to cv::Mat*
	// 2) Be careful, it is a void* not a void !! Need to get the pointed data
	
	//cv::Mat image =  ????
	Mat image = &userdata;
	width = image.size().width ;
	height = image.size().height ;
	if (width < 9) || (height < 9)
		break;
	// need to clone to avoid overwriting input data
    cv::Mat image_out = image.clone();
	
    // Mean on the neighborhood
	left = max(0,x-4.5);
	high = max(0, y - 4.5);
	right = min( width, x+4.5);
	low = min( height, y+4.5);

	// write your code

	cout << left;
	cout << "cacca";
	cout << x;
	cout << y;

    // Color segmentation
    
    // write your code

    cv::imshow("final_result", image_out);
    cv::waitKey(0);
  }

}

int main(int argc, char** argv)
{
   // read image into cv::Mat input_img and visualize
     
    // write your code

  Mat input_img = imread(argv[1]);
  namedWindow("Homework 1");
  imshow("Homework 1", input_img);
  
  cv::setMouseCallback("img", onMouse, (void*)&input_img);
  cv::waitKey(0);
  
  
  cv::destroyAllWindows();
  return 0;
}


