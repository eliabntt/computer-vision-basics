#ifndef PANORAMIC_H
#define PANORAMIC_H
#include <string>
#include <vector>
#include <opencv2/core/mat.hpp>


class Panoramic
{
public:
	Panoramic();
	Panoramic(std::string path, std::string extension) throw (std::exception);
	~Panoramic();

	std::vector<double> matching(cv::Mat input_1, cv::Mat input_2);
	std::vector<cv::DMatch> refine(std::vector<cv::DMatch> input_DMatch, double ratio);

	cv::Mat getImg(int index) throw (std::exception);
	int getNumber();

private:
	std::vector<cv::Mat> img;
	std::vector<cv::String> path;
	int total;
};

#endif // PANORAMIC_H
