#include "panoramic_utils.h"
#include "Panoramic.h"
#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{

	if (argc < 3 | argc > 3)
	{
		cout << "Not right # of arguments\n";
		cout << "Need the path with the images, the extension(jpeg != jpg)\n";
		cout << "press any key to finish.." << endl;
		cin.ignore();
		return -1;
	}
	if (argc == 3)
		cout << "# of arguments ok" << endl;
	Panoramic c;
	try
	{
		c = Panoramic(argv[1], argv[2]);
	}
	catch (const std::exception& e)
	{
		cout << e.what() << endl;
		cout << "Closing";
		cin.ignore();
		return -1;
	}

	Mat final_translated;
	try
	{
		final_translated = (PanoramicUtils::cylindricalProj(c.getImg(0), 33));
	}
	catch (const std::exception& e)
	{
		cout << e.what() << endl;
		cout << "Closing";
		cin.ignore();
		return -1;
	}

	for (int i = 1; i < c.getNumber(); i++)
	{
		//Here the getImg cannot throw exceptions
		Mat input_1(PanoramicUtils::cylindricalProj(c.getImg(i - 1), 33));
		Mat input_2(PanoramicUtils::cylindricalProj(c.getImg(i), 33));
		vector<double> output;
		try
		{
			output = c.matching(input_1, input_2);
		}
		catch (const std::exception& e)
		{
			cout << e.what() << endl;
			cout << "Closing";
			cin.ignore();
			return -1;
		}

		Mat temp = final_translated;

		resize(final_translated, final_translated, Size(final_translated.size().width + input_2.size().width - output.at(0), final_translated.size().height + abs(output.at(1))));

		if (output.at(1) > 0)
		{
			temp.copyTo(final_translated(Rect(0, 0, temp.size().width, temp.size().height)));
			input_2.copyTo(final_translated(Rect(temp.size().width - output.at(0), output.at(1), input_2.size().width, input_2.size().height)));
		}
		else
		{
			temp.copyTo(final_translated(Rect(0, output.at(1), temp.size().width, temp.size().height)));
			input_2.copyTo(final_translated(Rect(temp.size().width - output.at(0), 0, input_2.size().width, input_2.size().height)));
		}
		string title = "";
		if (i < c.getNumber() - 1)
			title = "Step";
		else title = "Final Image";
		imshow(title, final_translated);
		waitKey(0);
		destroyAllWindows();
	}
	imwrite("Final_panorama.jpg", final_translated);
	imshow("Final image", final_translated);
	waitKey(0);
	destroyAllWindows();
	return 0;
}
