#include "Panoramic.h"
#include <iostream>
#include <opencv2/core/core.hpp>
#include "panoramic_utils.h"
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv/cv.h>
#include <opencv2/xfeatures2d.hpp>
#include <iostream>

using namespace cv;
using namespace std;

/// <summary>
/// Check the validity of the input
/// </summary>
/// <param name="quest">The fixed string to be asked</param>
/// <param name="lower">the lower-1 admitted value</param>
/// <param name="upper">the upper+1 admitted value</param>
/// <returns></returns>
template <typename T>
static T check(string quest, T lower, T upper)
{
	bool finish = false;
	T value;
	while (!finish)
	{
		cout << quest << endl;
		cin >> value;
		if ((lower < value) & (value < upper))
			finish = true;
		else
		{
			cin.clear();
			cin.ignore();
		}
	}
	return value;
}

Panoramic::Panoramic() : total(0)
{
}

Panoramic::~Panoramic()
{
}

/// <summary>
/// Get the images
/// </summary>
/// <param name="path">location of the images</param>
/// <param name="extension">extension of the images</param>
Panoramic::Panoramic(string path, string extension) throw (exception)
{
	//need String from cv because of glob
	vector<String> res;
	String loc = path + "/*" + extension;

	//with glob one can find the files within the location(note the wildcard *)
	//and put the result on res. false/true is for recursive
	glob(loc, res, false);
	for (size_t k = 0; k < res.size(); ++k)
	{
		this->path.push_back(res[k]);
	}
	//sort the images so I can get in order
	sort(this->path.begin(), this->path.end());

	for (size_t k = 0; k < res.size(); ++k)
	{
		cout << this->path[k] << endl;
		Mat im = imread(this->path[k]), lapl;
		if (im.empty()) continue;

		resize(im, im, Size(), 0.5, 0.5);
		img.push_back(im);
		//save only the picture name with extension
	}
	total = img.size();

	if (total == 0)
		throw invalid_argument("Images not found exception");
}

/// <summary>
/// Get image given the index. Throw IndexOutOfRangeException.
/// </summary>
/// <param name="index">the index of the image</param>
/// <returns>The image Mat</returns>
Mat Panoramic::getImg(int index) throw (exception)
{
	if (index > total)
	{
		cout << "I do not have enough this image" << endl;
		throw invalid_argument("IndexOutOfRangeException");
	}
	return img.at(index);
}

/// <summary>
/// Get the number of the images
/// </summary>
/// <returns>The number of the images</returns>
int Panoramic::getNumber()
{
	return total;
}

/// <summary>
/// Do the matching between two BGR images
/// </summary>
/// <param name="input_1">The first BGR image</param>
/// <param name="input_2">The second BGR image</param>
/// <returns>A vector of transliations between inliers</returns>
vector<double> Panoramic::matching(Mat input_1, Mat input_2)
{

	Ptr<xfeatures2d::SIFT> sift_1 = xfeatures2d::SIFT::create();
	Ptr<xfeatures2d::SIFT> sift_2 = xfeatures2d::SIFT::create();
	vector<KeyPoint> key_points_1, key_points_2;
	Mat descriptors_1, descriptors_2;

	sift_1->detect(input_1, key_points_1, descriptors_1);
	sift_1->compute(input_1, key_points_1, descriptors_1);

	sift_2->detect(input_2, key_points_2, descriptors_2);
	sift_2->compute(input_2, key_points_2, descriptors_2);

	Mat feature_1, feature_2;
	drawKeypoints(input_1, key_points_1, feature_1);
	imshow("Img 1 keypoints", feature_1);
	drawKeypoints(input_2, key_points_2, feature_2);
	imshow("Img 2 keypoints", feature_2);

	waitKey(0);
	destroyAllWindows();

	string quest = "Which is the weight to apply to the min distance?[>1 and < 100]";
	double thre = check<double>(quest, 1, 100);

	BFMatcher matcher;
	vector<DMatch> matches;
	matcher.match(descriptors_1, descriptors_2, matches);

	vector<DMatch> refined = refine(matches, thre);

	vector<Point2f> img_1, img_2;
	vector<KeyPoint> new_key_points_1, new_key_points_2;
	for (auto& i : refined)
	{
		//since I've called descriptors_1, descriptors_2 in match
		//queryIdx is from input_1 and trainIdx is from input_2
		img_1.push_back(key_points_1[i.queryIdx].pt);
		new_key_points_1.emplace_back(img_1.back(), 1.f);
		img_2.push_back(key_points_2[i.trainIdx].pt);
		new_key_points_2.emplace_back(img_2.back(), 1.f);
	}

	drawKeypoints(input_1, new_key_points_1, feature_1);
	imshow("New Img 1 keypoints", feature_1);
	drawKeypoints(input_2, new_key_points_2, feature_2);
	imshow("New Img 2 keypoints", feature_2);

	waitKey(0);
	destroyAllWindows();

	Mat mask;
	int count = 0;
	Mat temp = findHomography(img_1, img_2, mask, RANSAC);
	double avg_x(0), avg_y(0);
	int maximum_x_diff(0), minimum_x_diff(input_1.size().width), maximum_y_diff(0), minimum_y_diff(input_1.size().height);
	for (int i = 0; i < mask.rows; i++)
	{
		if (mask.at<uchar>(i) == 1)
		{
			if (img_1.at(i).x - img_2.at(i).x > maximum_x_diff)
			{
				maximum_x_diff = img_1.at(i).x - img_2.at(i).x;
				maximum_y_diff = img_1.at(i).y - img_2.at(i).y;
			}
			if (img_1.at(i).x - img_2.at(i).x < minimum_x_diff)
			{
				minimum_x_diff = img_1.at(i).x - img_2.at(i).x;
				minimum_y_diff = img_1.at(i).y - img_2.at(i).y;
			}
			count = count + 1;
			avg_x = avg_x + (img_1.at(i).x - img_2.at(i).x);
			avg_y = avg_y + (img_1.at(i).y - img_2.at(i).y);
		}
	}
	vector<double> output(2);
	if (count > 5)
	{
		output.at(0) = (avg_x - maximum_x_diff - minimum_x_diff) / (count - 2);
		output.at(1) = (avg_y - maximum_y_diff - minimum_y_diff) / (count - 2);
	}
	else
	{
		output.at(0) = avg_x / count;
		output.at(1) = avg_y / count;
	}
	return output;
}

/// <summary>
/// Do the refinment. Compute the min distance then compare to a multiplicative factor.
/// </summary>
/// <param name="input_DMatch">The vector of matches</param>
/// <param name="ratio">The ratio to be keeped</param>
/// <returns>The new vector</returns>
vector<DMatch> Panoramic::refine(vector<DMatch> input_DMatch, double ratio)
{
	vector<DMatch> output;
	double min_distance = 100;
	for (auto& i : input_DMatch)
	{
		if (i.distance < min_distance)
			min_distance = i.distance;
	}

	for (auto& i : input_DMatch)
	{
		if (i.distance < min_distance*ratio)
			output.push_back(i);
	}

	return output;
}
