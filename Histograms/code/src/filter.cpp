#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui.hpp>
#include "filter.h"

using namespace cv;

// constructor
Filter::Filter(Mat input_img, int size_x, int size_y) {
	input_image = input_img;
	setSize(size_x, size_y);
}

// for base class do nothing (in derived classes it performs the corresponding filter)
void Filter::doFilter() {
	// it just returns a copy of the input image
	result_image = input_image.clone();
}

// get output of the filter
Mat Filter::getResult() {

	return result_image;
}

Mat Filter::getResult_c() {
	result_image_c.download(result_image);
	return result_image;
}


//set window size (it needs to be odd)
void Filter::setSize(int size_x, int size_y) {
	if (size_x % 2 == 0)
		size_x++;
	filter_size_x = size_x;
	if (size_y % 2 == 0)
		size_y++;
	filter_size_y = size_y;
}

//get window size 
int Filter::getSize_x() {
	return filter_size_x;
}

int Filter::getSize_y() {
	return filter_size_y;
}

//Gaussian
GaussianFilter::GaussianFilter(Mat input_img, int size_x, int size_y, double s_x, double s_y) :Filter(input_img, size_x, size_y)
{
	setSigma(s_x, s_y);
}

void GaussianFilter::setSigma(double s_x, double s_y) {
	sigma_x = s_x;
	sigma_y = s_y;
}

double  GaussianFilter::getSigma_x()
{
	return sigma_x;
}

double  GaussianFilter::getSigma_y()
{
	return sigma_y;
}

void GaussianFilter::doFilter()
{
	GaussianBlur(input_image, result_image, Size(filter_size_x, filter_size_y), sigma_x, sigma_y);
}

MedianFilter::MedianFilter(Mat input_img, int x) : Filter(input_img, x, x)
{}

void MedianFilter::doFilter() {
	medianBlur(input_image, result_image, filter_size_x);
}

BilateralFilter::BilateralFilter(Mat input_img, int size_x, double s_c, double s_s) : Filter(input_img, size_x, size_x)
{
	setSigmaS(s_s);
	setSigmaC(s_c);
}

void BilateralFilter::doFilter_c() {
	cuda::bilateralFilter(cuda::GpuMat(input_image), result_image_c, filter_size_x, sigma_color, sigma_space);
}

void BilateralFilter::doFilter() {
	bilateralFilter(input_image, result_image, -1, sigma_color, sigma_space);
}

void BilateralFilter::setSigmaC(double s_c) {
	sigma_color = s_c;
}

void BilateralFilter::setSigmaS(double s_s) {
	sigma_space = s_s;
}

double BilateralFilter::getSigmaC()
{
	return sigma_color;
}

double BilateralFilter::getSigmaS()
{
	return sigma_space;
}
