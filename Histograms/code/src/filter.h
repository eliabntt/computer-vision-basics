#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui.hpp>

using namespace cv;

class Filter {

public:
	Filter(Mat input_img, int filter_size_x, int filter_size_y);

	void doFilter();
	Mat getResult();

	Mat getResult_c();
	void setSize(int size_x, int size_y);
	int getSize_x();
	int getSize_y();

protected:

	Mat input_image;
	Mat result_image;
	int filter_size_x, filter_size_y;
	cuda::GpuMat result_image_c;
};


using namespace cv;
using namespace std;
// Gaussian Filter

class GaussianFilter : public Filter {
public:
	GaussianFilter(Mat input_img, int size_x, int size_y, double s_x, double s_y);
	void doFilter();
	void setSigma(double s_x, double s_y);
	double getSigma_x();
	double getSigma_y();
protected:
	double sigma_x, sigma_y;
};

class MedianFilter : public Filter {
public:
	MedianFilter(Mat input_img, int x);
	void doFilter();
};

class BilateralFilter : public Filter {
public:
	BilateralFilter(Mat input_img, int size_x, double s_c, double s_s);
	void doFilter();
	void doFilter_c();
	void setSigmaC(double s_c);
	void setSigmaS(double s_s);
	double getSigmaC();
	double getSigmaS();
	//~BilateralFilter();
protected:
	double sigma_color, sigma_space;

};
