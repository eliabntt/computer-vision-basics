#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui.hpp>

#include "filter.h"

using namespace std;
using namespace cv;

Mat src, img;
int type = -1, last = 0, m_sigma_x = 0, m_sigma_y = 0, m_size_x = 0, m_size_y = 0, m_elem = 0, m_size = 0;
int const max_elem = 3;
int const max_kernel_size = 31;
int const max_sigma_elem = 6;
void morph(int, void*);
void median(int, void*);
void gaussian(int, void *);
void bilateral(int, void *);
void bilateral_c(int, void *);

template <typename T>
inline vector<Mat> drawHist(vector<Mat> RGB_hist, vector<Mat> histImage, int height, int width, int bins_w, int bins)
{
	vector<Scalar> color(3);
	color.at(0) = Scalar(255, 0, 0);
	color.at(1) = Scalar(0, 255, 0);
	color.at(2) = Scalar(0, 0, 255);
	for (int i = 0; i < RGB_hist.size(); i++)
	{
		histImage.at(i) = Mat(height, width, CV_8UC3, Scalar(0, 0, 0));
		normalize(RGB_hist.at(i), RGB_hist.at(i), 0, histImage.at(i).rows, NORM_MINMAX, -1, Mat());
	}
	for (int j = 0; j < histImage.size(); j++)
		for (int i = 1; i <= bins; i++)
		{
			//since cuda::calcHist return CV_32SC1 aka 32bit signed Integer I use int to find the value at..
			line(histImage.at(j), Point(bins_w*(i - 1), height - (RGB_hist.at(j).at<T>(i - 1))),
				Point(bins_w*(i - 1), height),
				color.at(j), 2, 8, 0);
		}
	return histImage;
}

inline Mat plotHist(vector < Mat> histImage)
{
	Mat final_mat(histImage.at(0).size().height * 0.5, histImage.at(0).size().width * 3 * 0.5, CV_8UC3);

	for (int i = 0; i < histImage.size(); i++)
	{
		resize(histImage.at(i), histImage.at(i), Size(), 0.5, 0.5);
		histImage.at(i).copyTo(final_mat(Rect(i*histImage.at(i).size().width, 0, histImage.at(i).size().width, histImage.at(i).size().height)));
	}
	return final_mat;
}
/*
see how getstructuringelement works
and how parameters are passed to functions here especially create trackbar and morph
*/


int main(int argc, char** argv)
{
	if (argc < 2)
	{
		cout << "Not enough argument" << endl;
		cout << "Need the path of the picture(including name and extension)" << endl;
		cout << "press enter to finish.." << endl;
		cin.ignore();
		return -1;
	}
	else
		if (argc == 2)
			cout << "# of arguments ok" << endl;
		else
		{
			cout << "too much arguments" << endl;
			cout << "Need the path of the picture(including name and extension)" << endl;
			cout << "press enter to finish.." << endl;
			cin.ignore();
			return -1;
		}

	Mat image_normal = imread(argv[1]);

	//split the image into R,G, B channels
	//try with CUDA
	bool cuda = false;
	try
	{
		cuda::GpuMat image;
		cuda = true;
	}
	catch (const std::exception&)
	{
		cout << "cuda is not present" << endl;
	}

	vector<Mat> eq_image(3), bgr_planes, lab, RGB_hist(3), histImage(3);
	Mat final_eq, image_lab, equalized;
	int bins = 256;
	int hist_w = 512; int hist_h = 400;
	int bin_w = cvRound((double)hist_w / bins);
	float range[] = { 0, bins };
	const float* histRange = { range };
	bool uniform = true; bool accumulate = false;
	double cuda_split = 0, cuda_hist = 0, cuda_2lab = 0, cuda_eq_rgb = 0;
	clock_t tStart = clock();

	if (cuda)
	{
		cuda::GpuMat image;
		image.upload(image_normal);

		if (image.empty())
		{
			cout << "I cannot load the image" << endl;
			cout << "press enter to finish.." << endl;
			cin.ignore();
			return -1;
		}


#pragma region cuda

		vector<cuda::GpuMat> RGB, hist(3), eq_image_c(3), lab_c;
		cuda::GpuMat final_eq_c, image_lab_c;
		cuda::Stream s = cuda::Stream();
		tStart = clock();
		cuda::split(image, RGB, s);
		cuda_split = (double)(clock() - tStart);

		tStart = clock();
		for (int i = 0; i < 3; i++)
		{
			cuda::calcHist(RGB.at(i), hist.at(i), s);
			hist.at(i).download(RGB_hist.at(i));
		}
		cuda_hist = (double)(clock() - tStart);
		for (auto i : RGB_hist)
			i = i.t();

		histImage = drawHist<int>(RGB_hist, histImage, hist_h, hist_w, cvRound((double)hist_w / bins), bins);
		imshow("Hist CUDA", plotHist(histImage));
		waitKey(last);

		//equalize the single channel
		tStart = clock();
		for (int i = 0; i < RGB.size(); i++)
			cuda::equalizeHist(RGB.at(i), eq_image_c.at(i));

		//recompose the image
		cuda::merge(eq_image_c, final_eq_c, s);
		final_eq_c.download(equalized);
		cuda_eq_rgb = (double)(clock() - tStart);

		imshow("CUDA equalized", equalized);
		waitKey(last);

		//hist of the equalized
		cuda::split(final_eq_c, RGB);
		for (int i = 0; i < 3; i++)
		{
			cuda::calcHist(RGB.at(i), hist.at(i), s);
			hist.at(i).download(RGB_hist.at(i));
			RGB_hist.at(i) = RGB_hist.at(i).t();
		}

		histImage = drawHist<int>(RGB_hist, histImage, hist_h, 512, cvRound((double)hist_w / bins), bins);
		imshow("Hist CUDA_eq", plotHist(histImage));
		waitKey(last);

		//change the image to lab
		tStart = clock();
		cuda::cvtColor(image, image_lab_c, CV_BGR2Lab);
		cuda::split(image_lab_c, lab_c, s);
		cuda::equalizeHist(lab_c.at(0), eq_image_c.at(0));
		eq_image_c.at(2) = lab_c.at(2);
		eq_image_c.at(1) = lab_c.at(1);
		//recompose the image
		cuda::merge(eq_image_c, final_eq_c, s);
		cuda::cvtColor(final_eq_c, final_eq_c, CV_Lab2BGR);
		final_eq_c.download(equalized);
		cuda_2lab = (double)(clock() - tStart);

		imshow("CUDA-Lab equalized", equalized);
		waitKey(last);

		//hist of the equalized
		cuda::split(final_eq_c, RGB);
		for (int i = 0; i < 3; i++)
		{
			cuda::calcHist(RGB.at(i), hist.at(i), s);
			hist.at(i).download(RGB_hist.at(i));
			RGB_hist.at(i) = RGB_hist.at(i).t();
		}

		histImage = drawHist<int>(RGB_hist, histImage, hist_h, 512, cvRound((double)hist_w / bins), bins);
		imshow("Hist CUDA-Lab_eq", plotHist(histImage));
		waitKey(last);

		destroyAllWindows();
#pragma endregion cuda first points with CUDA
	}

#pragma region cpu 
	tStart = clock();
	split(image_normal, bgr_planes);
	double cpu_split = (double)(clock() - tStart);

	tStart = clock();
	for (int i = 0; i < bgr_planes.size(); i++)
		calcHist(&bgr_planes[i], 1, 0, Mat(), RGB_hist.at(i), 1, &bins, &histRange, uniform, accumulate);
	double cpu_hist = (double)(clock() - tStart);

	histImage = drawHist<float>(RGB_hist, histImage, hist_h, 512, cvRound((double)hist_w / bins), bins);
	imshow("Hist CPU", plotHist(histImage));
	waitKey(last);

	tStart = clock();
	for (int i = 0; i < eq_image.size(); i++)
		equalizeHist(bgr_planes.at(i), eq_image.at(i));
	//recompose the image
	merge(eq_image, final_eq);
	double cpu_eq_hist = (double)(clock() - tStart);

	imshow("CPU equalized", final_eq);
	waitKey(last);

	//hist of the equalized
	split(final_eq, bgr_planes);
	for (int i = 0; i < bgr_planes.size(); i++)
		calcHist(&bgr_planes[i], 1, 0, Mat(), RGB_hist.at(i), 1, &bins, &histRange, uniform, accumulate);
	histImage = drawHist<float>(RGB_hist, histImage, hist_h, 512, cvRound((double)hist_w / bins), bins);
	imshow("Hist CPU_eq", plotHist(histImage));
	waitKey(last);

	tStart = clock();
	cvtColor(image_normal, image_lab, CV_BGR2Lab);
	split(image_lab, lab);
	equalizeHist(lab.at(0), eq_image.at(0));
	eq_image.at(2) = lab.at(2);
	eq_image.at(1) = lab.at(1);
	//recompose the image
	merge(eq_image, final_eq);
	cvtColor(final_eq, final_eq, CV_Lab2BGR);
	double cpu_2lab = (double)(clock() - tStart);

	imshow("CPU-Lab equalized", final_eq);
	waitKey(last);
	//hist of the equalized
	split(final_eq, bgr_planes);
	for (int i = 0; i < bgr_planes.size(); i++)
		calcHist(&bgr_planes[i], 1, 0, Mat(), RGB_hist.at(i), 1, &bins, &histRange, uniform, accumulate);
	histImage = drawHist<float>(RGB_hist, histImage, hist_h, 512, cvRound((double)hist_w / bins), bins);
	imshow("Hist CPU-Lab_eq", plotHist(histImage));
	waitKey(last);
	destroyAllWindows();

#pragma endregion cpu first points with cpu	

	printf("Time taken for split: %.3fs  [CUDA] vs  %.3fs [CPU]\n", cuda_split / CLOCKS_PER_SEC, cpu_split / CLOCKS_PER_SEC);
	printf("Time taken to calc hist: %.3fs  [CUDA] vs  %.3fs [CPU]\n", cuda_hist / CLOCKS_PER_SEC, cpu_hist / CLOCKS_PER_SEC);
	printf("Time taken to eq rgb: %.3fs  [CUDA] vs  %.3fs [CPU]\n", cuda_eq_rgb / CLOCKS_PER_SEC, cpu_eq_hist / CLOCKS_PER_SEC);
	printf("Time taken to do the whole 2lab: %.3fs  [CUDA] vs  %.3fs [CPU]\n", cuda_2lab / CLOCKS_PER_SEC, cpu_2lab / CLOCKS_PER_SEC);


#pragma region morph
	bool finish = false;
	while (!finish)
	{
		while (!finish)
		{
			cout << "What kind of Type do you want?[requesting the int]" << endl << "0: Rect" << endl << "1: Cross" << endl << "2: Ellipse" << endl;
			cin >> type;
			if ((-1 < type) & (type < 3))
				finish = true;
			else
			{
				cin.clear();
				cin.ignore();
			}
		}
		finish = false;

		cin.clear();
		cout << endl;
		cout << "Since there is a \"Bug\" on OpenCV that truncate the strings longer than 10 chars please note that\n E = Erosion \n D = Dilation \n MO = Morph Open \n MC = Morph Close \n Enter to go on" << endl;
		getchar();
		cin.ignore();

		resize(final_eq, src, Size(), 0.5, 0.5);
		namedWindow("CPU_morph", WINDOW_NORMAL);
		resizeWindow("CPU_morph", src.size().width, src.size().height);
		//bug on too long text, need to change a flag and recompile
		//so shorter version
		createTrackbar("E-D-MO-MC", "CPU_morph",
			&m_elem, max_elem,
			morph);
		createTrackbar("Kernel", "CPU_morph",
			&m_size, max_kernel_size,
			morph);
		morph(0, 0);
		waitKey(last);
		destroyAllWindows();
		m_elem = 0, m_size = 0;
		cout << "Want to retry? Y/n" << endl;
		string again;
		cin >> again;
		if (again == "n" | again == "N")
			finish = true;
		type = -1;
	}
#pragma endregion morph morphology with trackbar and while, CPU-STYLE // TODO WATCH IF TODO WITH CUDA


	finish = false;

	while (!finish)
	{
		while (!finish)
		{
			cout << "What filter do you want now?[requesting the int]" << endl << "0: Median" << endl << "1: Gaussian" << endl << "2: Bilateral CUDA" << endl << "3: Bilateral CPU" << endl;
			cin >> type;
			if ((-1 < type) & (type <= 3))
				finish = true;
			else
			{
				cin.clear();
				cin.ignore();
			}
		}
		finish = false;

		switch (type)
		{
		case 0:
			namedWindow("Median", WINDOW_NORMAL);
			createTrackbar("Kernel", "Median",
				&m_size, max_kernel_size,
				median);
			median(0, 0);
			break;
		case 1:
			namedWindow("Gaussian", WINDOW_NORMAL);
			createTrackbar("Sigma x", "Gaussian",
				&m_sigma_x, max_sigma_elem,
				gaussian);
			createTrackbar("Sigma y", "Gaussian",
				&m_sigma_y, max_sigma_elem,
				gaussian);
			createTrackbar("Kernel x", "Gaussian",
				&m_size_x, max_kernel_size,
				gaussian);
			createTrackbar("Kernel y:", "Gaussian",
				&m_size_y, max_kernel_size,
				gaussian);
			gaussian(0, 0);
			break;
		case 2:
			//MUST use CUDA
			namedWindow("Bilateral", WINDOW_NORMAL);
			resizeWindow("Bilateral", src.size().width, src.size().height);
			createTrackbar("Sigma Color", "Bilateral",
				&m_sigma_x, max_sigma_elem * 30,
				bilateral_c);
			createTrackbar("Sigma Space", "Bilateral",
				&m_sigma_y, max_sigma_elem * 30,
				bilateral_c);
			createTrackbar("Kernel dim", "Bilateral",
				&m_size_x, max_kernel_size,
				bilateral_c);
			bilateral_c(0, 0);
			break;
		
		case 3:
			//MUST use CUDA
			namedWindow("Bilateral", WINDOW_NORMAL);
			resizeWindow("Bilateral", src.size().width, src.size().height);
			createTrackbar("Sigma color", "Bilateral",
				&m_sigma_x, max_sigma_elem * 30,
				bilateral);
			createTrackbar("Sigma space", "Bilateral",
				&m_sigma_y, max_sigma_elem * 30,
				bilateral);
			bilateral(0, 0);
			break;
		default:
			break;
		}

		waitKey(0);
		destroyAllWindows();
		m_sigma_x = 0, m_sigma_y = 0, m_size_x = 0, m_size_y = 0;
		cout << "Want to retry? Y/n" << endl;
		string again;
		cin >> again;
		if (again == "n" | again == "N")
			finish = true;
		type = -1;
	}

	return 0;

}


void morph(int, void*)
{
	int shape = type;

	Mat element = getStructuringElement(shape,
		Size(2 * m_size + 1, 2 * m_size + 1),
		Point(m_size, m_size));

	if (m_elem == 0)
	{
		erode(src, img, element);
	}
	if (m_elem == 1)
	{
		dilate(src, img, element);
	}
	if (m_elem == 2)
	{
		morphologyEx(src, img, MORPH_OPEN, element);
	}
	if (m_elem == 3)
	{
		morphologyEx(src, img, MORPH_CLOSE, element);
	}

	imshow("CPU_morph", img);
}

void gaussian(int, void *)
{
	GaussianFilter filt = GaussianFilter(src, m_size_x, m_size_y, m_sigma_x, m_sigma_y);
	filt.doFilter();
	imshow("Gaussian", filt.getResult());
}

void bilateral_c(int, void *)
{
	BilateralFilter filt = BilateralFilter(src, -1, m_sigma_x, m_sigma_y);
	filt.doFilter_c();
	imshow("Bilateral", filt.getResult_c());
}

void bilateral(int, void *)
{
	BilateralFilter filt = BilateralFilter(src, -1, m_sigma_x, m_sigma_y);
	filt.doFilter();
	imshow("Bilateral", filt.getResult());
}

void median(int, void *)
{
	MedianFilter filt = MedianFilter(src, m_size);
	filt.doFilter();
	imshow("Median", filt.getResult());
}
